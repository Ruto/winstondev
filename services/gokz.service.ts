import axios from 'axios';
import Map from '@/models/gokz/map.model';

export default {
  getRecentTimes: (n: number = 50) => {},

  getMaps: async (): Promise<Map[]> => {
    console.log('Called gokz.getMaps');
    try {
      //return (await axios.get('http://localhost:5000/api/gokz/maps'))
      return (await axios.get('https://devruto.com/api/maps'))
        .data as Map[];
    } catch (err) {
      console.log(err);
      return Promise.resolve([] as Map[]);
    }

    // return Promise.resolve([
    //   {
    //     id: 0,
    //     name: 'kz_reach_v2',
    //     last_played: new Date('2019-09-09'),
    //     created: new Date('1996-01-01'),
    //     in_ranked_pool: 1
    //   },
    //   {
    //     id: 1,
    //     name: 'kz_eros_v2',
    //     last_played: new Date('2019-09-09'),
    //     created: new Date('1996-01-01'),
    //     in_ranked_pool: 1
    //   },
    //   {
    //     id: 2,
    //     name: 'kz_something',
    //     last_played: new Date('2019-09-09'),
    //     created: new Date('1996-01-01'),
    //     in_ranked_pool: 1
    //   },
    //   {
    //     id: 3,
    //     name: 'kz_noon',
    //     last_played: new Date('2019-09-09'),
    //     created: new Date('1996-01-01'),
    //     in_ranked_pool: 1
    //   }
    // ]);
  }
};

import axios from 'axios';
import { Replay } from '~/models/replay.model';
const ENDPOINT = 'https://ruto.sh/api/replays/';

export default {
  async getRecents(): Promise<Replay[]> {
    return (await axios.get(ENDPOINT)).data as Replay[];
  },

  async getByMap(map: string) {
    return (await axios.get(ENDPOINT, {
      params: {
        map: map
      }
    })).data as Replay[];
  },

  async downloadReplay(id: number) {
    return (await axios.get(ENDPOINT + id)).data;
  }
};

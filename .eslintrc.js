module.exports = {
  plugins: ['@typescript-eslint'],
  parserOptions: {
    parser: '@typescript-eslint/parser'
  },
  extends: ['@nuxtjs'],
  rules: {
    semi: 'off',
    '@typescript-eslint/semi': ['error', 'always'],
    '@typescript-eslint/no-unused-vars': ['off'],
    '@typescript-eslint/no-useless-constructor': 'off',
    'no-useless-constructor': 'off',
    'no-console': 'off',
    'no-lone-blocks': 'off',
    'no-unused-vars': 'off',
    camelcase: 'off',
    quotes: ['error', 'single'],
    'standard/no-callback-literal': 'off',
    eqeqeq: 'off',
    'no-dupe-class-members': ['warn'],
    'no-return-assign': 'off',
    'no-irregular-whitespace': 'off',
    'no-undef': 'off',
    'no-var': 'off',
    'no-throw-literal': 'off',
    'handle-callback-err': 'off',
    'new-cap': 'off',
    'no-tabs': 'off',
    'no-useless-escape': 'off',
    'prefer-const': ['warn'],
    'require-await': 'off'
  }
};

export default interface Map {
  map_id: number;
  name: string;
  last_played: Date;
  created: Date;
  in_ranked_pool: number;
}

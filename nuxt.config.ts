import NuxtConfiguration from '@nuxt/config';

const config: NuxtConfiguration = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src: '/js/facepunch.webgame.js',
        type: 'text/javascript'
      },
      {
        src: '/js/sourceutils.js',
        type: 'text/javascript'
      },
      {
        src: '/js/replayviewer.js',
        type: 'text/javascript'
      }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    './node_modules/bulmaswatch/darkly/bulmaswatch.scss',
    './node_modules/@fortawesome/fontawesome-free/css/all.css',
    './node_modules/bulma-slider/dist/css/bulma-slider.min.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: ['~/plugins/vuetable.js', '~/plugins/vuemoment.js'],
  /*
   ** Nuxt.js modules
   */
  modules: ['nuxt-buefy'],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    host: 'localhost',
    port: 5000,
    prefix: '/api/'
  },
  /*
   ** Build configuration
   */
  build: {
    postcss: {
      preset: {
        features: {
          customProperties: false
        }
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {}
  }
};

export default config;
